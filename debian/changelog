debian-history (2.25) unstable; urgency=medium

  [ Boyuan Yang ]
  * Non-maintainer upload from git repo with acknowledgement from
    the maintainer to fix current DPL information. (Closes: #970242)
  * Bump Standards-Version to 4.5.0.

  [ Holger Wansing ]
  * Fix number of developers when Sarge was released. Closes: #925152
  * Add Debconf15 - Debconf19.
  * Update information regarding project leader vote 2020.

  [ Sangdo Jun ]
  * Update of Korean translation.

  [ Martin Michlmayr ]
  * Fix typos and cosmetic issues.

 -- Boyuan Yang <byang@debian.org>  Mon, 14 Sep 2020 09:50:29 -0400

debian-history (2.24) unstable; urgency=medium

  [ Javier Fernández-Sanguino Peña ]
  * Updated spanish translation
  * Add content of the Debian 10 «buster» release.
  * debian/copyright: Modify to be in sync with information in document.
    Also include proper copyright/license header in PO files which missed it
    or only included the translator's
  * Updated French translation (Closes: #928109)
    - Create also po4a/add_fr/project-history.add to credit the translation
  * Remove the links to libresoft.es (no longer available) and change the
    link of the article to an online source (Closes: #925093)
  * Remove the links to Alioth (Closes: #925147)
  * Review the references to Debconfs, add Debconf0 and adjust the numbering
    of the Debconf's which were not correly numbered. Also add links to
    the Debconf sites which are all still available 
    (Closes: #925341, #925367, #926258, #926260)
  * Document Steve Greenland's death in 2009 (Closes: #815225)

  [ Holger Wansing ]
  * Update the information related to Project Leaders (2017 - 2019).

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Drop custom source compression.
  * Remove empty debian/source/options.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Fabrice BAUZAC-STEHLY ]
  * Fix Emacs version in Stretch.

  [ Osamu Asoki ]
  * Updated Portuguese translation by Américo Monteiro (Closes: #924735)

 -- Javier Fernandez-Sanguino <jfs@debian.org>  Sat, 18 Apr 2020 12:29:39 +0200

debian-history (2.23) unstable; urgency=medium

  * Fix build system for date string and use id.as.filename.
  * Merge patch by Holger Wansing for machine compromise and
    point release.  Update TODO. Closes: #360623, #810575
  * Pick stable section id names.

 -- Osamu Aoki <osamu@debian.org>  Sun, 23 Sep 2018 23:14:12 +0900

debian-history (2.22) unstable; urgency=medium

  [ Martin Michlmayr ]
  * Change link of printed Debian announcement from archive.org to
    flickr.com.
  * List Chris Lamb as new DPL.  Closes: #860759

  [ Steve Petruzzello ]
  * Update french translation

  [ Holger Wansing ]
  * Update german translation
  * Change codename for testing from stretch to buster.

  [ Javier Fernández-Sanguino Peña ]
  * Update information for the Debian 9 «stretch» release.

  [ Osamu Aoki ]
  * Migrate to DocBook XML 4.5.
  * Move all translation to po4a with workaround for <bookinfo> tag.
  * Build text and epub but drop PS.

 -- Osamu Aoki <osamu@debian.org>  Wed, 05 Sep 2018 00:24:34 +0900

debian-history (2.21) unstable; urgency=medium

  [ Martin Michlmayr ]
  * List Mehdi Dogguy as new DPL.
  * Remove Ian Murdock's email address.
  * Use web.archive.org for the link to ianmurdock.com
  * Remove some architectures that are no longer supported.
  * Add section dedicated to Kristoffer H. Rose.

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Jan 2017 15:01:55 -0800

debian-history (2.20) unstable; urgency=medium

  [ Javier Fernández-Sanguino Peña ]
  * project-history: Fix dead links to "Debian counting site" and typo with
    patch provided by Daniele Forsi (Closes: #734274)

  [ David Prévot ]
  * Work around #725931

  [ Baptiste Jammet ]
  * List Neil McGovern as new DPL, thanks Jakub Wilk.  Closes: #797206

  [ Martin Michlmayr ]
  * The next release is stretch, not jessie.  Thanks to Jack van Klaren.
  * Fix some typos.
  * Fix capitalization of some words.
  * Fix grammatical error, thanks Christopher Hagar.  Closes: #742567
  * Correction on Debian's release policy: Debian adopted time-based
    freezes, not time-based releases.
  * Add information about DebConf13.
  * Replace link to announcement of Debian with plain text version.
    Closes: #766753
  * Update http links to https where sites redirect to https.

  [ Holger Wansing]
  * Add info about deaths since 2010 to "Important Events" section.
    Closes: #770656
  * Add info about the deaths of Clytie Siddall and Ian Murdock.
  * Add portuguese translation by Américo Monteiro. Closes: #756292
  * Mention Ian's scanned printout of Debian announcement. Closes: #810574

 -- Javier Fernández-Sanguino Peña <jfs@debian.org>  Fri, 19 Feb 2016 19:58:36 +0100

debian-history (2.19~deb7u1) stable; urgency=low

  * Rebuild for Wheezy

 -- Javier Fernández-Sanguino Peña <jfs@debian.org>  Sun, 02 Jun 2013 00:40:37 +0200

debian-history (2.19) unstable; urgency=low

  [ Javier Fernandez-Sanguino ]
  * Updated for the Wheezy release

  [ Translation updates ]
  * French, Christian Perrier, Steve Petruzzello
  * German, Chris Leick, closes: #706719
  * Japanese, victory
  * Russian, Sergey Alyoshin, closes: #708941

 -- David Prévot <taffit@debian.org>  Thu, 02 May 2013 17:30:33 -0400

debian-history (2.18) unstable; urgency=low

  [ David Prévot ]
  * Clean up po4a part of the build, using its own configuration file.

  [ Sergey Alyoshin ]
  * Correct text wrap in Russian text file.

  [ Translation updates ]
  * Russian, Sergey Alyoshin

  [ Bdale Garbee ]
  * note new DPL
  * several minor updates and grammatical corrections

 -- Bdale Garbee <bdale@gag.com>  Wed, 24 Apr 2013 23:00:44 -0600

debian-history (2.17) unstable; urgency=low

  * Team upload

  [ Javier Fernandez-Sanguino ]
  * Review the Squeeze introduction based on comments in the debian-doc
    mailing list (mainly coming from Justin B Rye).
  * Review the use of 'in the movie' throughout the document.

  [ Translation updates ]
  * French, Steve Petruzzello, closes: #669674
  * German, Chris Leick, closes: #684087
  * Japanese, YOSHINO Yoshihito, closes: #684328

  [ David Prévot ]
  * Fix typos spotted by Sergey Alyoshin and Chris Leick
  * Makefile: Keep previous msgids of translated messages
  * Remove inaccurate part about skipping Squeeze upgrade
  * debian/control: Bump standards version (no change needed)
  * debian/copyright: Point to the GPL-2 file
  * debian/source/options: Switch to xz compression for source

 -- David Prévot <taffit@debian.org>  Fri, 10 Aug 2012 12:49:14 -0400

debian-history (2.16) unstable; urgency=low

  [ Bdale Garbee ]
  * fix typo, closes: #653734
  * new German translation, closes: #611017
  * add a paragraph honoring i-Connect.Net's early contributions, including
    hosting master.debian.org for a while after it left HP, closes: #638218

  [ Javier Fernandez-Sanguino ]
  * Make visible the short introduction of Squeeze (was commented out and
    nobody remembered to uncomment it in the previous upload)

 -- Bdale Garbee <bdale@gag.com>  Fri, 30 Dec 2011 09:46:30 -0700

debian-history (2.15) unstable; urgency=low

  [ Ryan Kavanagh ]
  * Applied Chris Leick's typo fix patch, closes: #611021
  * debian-history.doc-base.ko should refer to Korean documentation, not
    Lithuanian. closes: #610018
  * Added section on the passing of Frans Pop, closes: #611024
  * Next release is wheezy, updated the "What's Next?" section accordingly
  * Uncommented and updated the Squeeze section

  [ Osamu Aoki ]
  * Fixed build issues for the Italian by removing character entities.

 -- Osamu Aoki <osamu@debian.org>  Sat, 23 Jul 2011 23:11:54 +0900

debian-history (2.14) unstable; urgency=low

  [ David Prévot ]
  * Updated French translation from Steve Petruzzello, closes: #599182

  [ Osamu Aoki ]
  * Retroactively added note on #594538 and Lithuanian missed in 2.13.
  * Fixed leadership text consistency and Andrés García.

 -- Osamu Aoki <osamu@debian.org>  Thu, 09 Dec 2010 09:56:27 +0900

debian-history (2.13) unstable; urgency=low

  [ Javier Fernandez-Sanguino ]
  * Include some information of the Lenny release.
   * Add information in preparation for the Squeeze Release
   * Add information of the "last" Debconf conferences (2007 onwards),
    including links to the videos, pictures and the group photo
   * Add references in comments as TODO to include additional information of
     the Lenny release as well as pointers to other documents
     (reports of Debconfs to add more information on them too)

  [ Osamu Aoki ]
  * Converted to UTF-8 for all languages, closes: #594538
  * Updated the German PO file
  * doc-base and installed files adjusted for Korean, Japanese,
    and Lithuanian
  * Added Uploaders, Homepage, Vcs-Svn, and Vcs-Browser fields
    to debian/control
  * Updated package dependency for Squeeze.
  * Fixed typos reported by David Richfield, closes: #541957
  * Updated the Lithuanian PO file translation from Kęstutis Biliūnas.
  * Added lmodern font for Lithuanian.

 -- Osamu Aoki <osamu@debian.org>  Sun, 12 Sep 2010 08:49:24 +0900

debian-history (2.12) unstable; urgency=low

  [Martin Zobel-Helas]
  * zack is now our current DPL.

  [Bdale Garbee]
  * add lmodern to build deps to solve font availability FTBFS

 -- Bdale Garbee <bdale@gag.com>  Sat, 15 May 2010 19:34:41 +0000

debian-history (2.11) unstable; urgency=low

  * update build dependencies in response to elimination of littex
  * updated Italian translation from Ferdinando Ferranti
  * merge content lost in CVS to SVN ddp transition

  [ Javier Fernandez-Sanguino ]
  * Fix links to online articles
  * Add section dedicated to Thiemo Seufer

 -- Bdale Garbee <bdale@gag.com>  Fri, 19 Mar 2010 23:35:05 -0600

debian-history (2.10) unstable; urgency=low

  * fix typos found by Helge Kreutzmann, closes: #445634
  * incorporate results of recent DPL election

 -- Bdale Garbee <bdale@gag.com>  Wed, 16 Apr 2008 00:43:08 -0600

debian-history (2.9) unstable; urgency=low

  * merge Lithuanian translation, closes: #437059
  * fix date in Spanish translation, closes: #426602, #426603
  * clean up postinst and prerm since debhelper gets it right, closes: #424730

 -- Bdale Garbee <bdale@gag.com>  Mon, 20 Aug 2007 09:54:08 -0600

debian-history (2.8) unstable; urgency=low

  * incorporate patch from Nicolas Francois.  Cleans up build dependencies,
    moves from po-debconf to po4a, fixes hardcoded list of languages.
    closes: #420332
  * transition from using tetex to texlive

 -- Bdale Garbee <bdale@gag.com>  Wed, 16 May 2007 10:22:51 -0600

debian-history (2.7) unstable; urgency=high

  * crank the urgency since this should release with etch
  * turn on pdf and ps delivery for noncjk locales, closes: #273365
  * pick up changes in CVS, closes: #382636
  * merge updated Japanese translation, closes: #356281
  * freshen standards and lintian versions

  [ Javier Fernandez-Sanguino ]
  * Update project leader info
  * Add current leader and info on Debconf6

 -- Bdale Garbee <bdale@gag.com>  Wed,  4 Apr 2007 01:11:17 -0600

debian-history (2.6) unstable; urgency=low

  * freshen to pick up changes in CVS, closes: #305039, #356281, #263807
    closes: #226676, #179978, #169589, #186504, #203031
  * fix a few out-dated assertions, closes: #299767
  * update version info on the title page

  [ Javier Fernandez-Sanguino ]
  * Update the information related to Project Leaders (added Branden) as
    well as the information related to the sarge release and etch release goals
  * Updated for the sarge release
  * Minor corrections and typos


 -- Bdale Garbee <bdale@gag.com>  Thu, 23 Mar 2006 10:30:23 -0700

debian-history (2.5) unstable; urgency=low

  * merge NMU changes into CVS
  * fix FTBFS problem, closes: #269986
  * merge patch from Ingo Saitz to add references to slink, closes: #218458
  * various cosmetic fixes to sources

 -- Bdale Garbee <bdale@gag.com>  Mon,  6 Sep 2004 02:26:34 -0600

debian-history (2.4-0.1) unstable; urgency=low

  * NMU (2.3 and 2.4 were never uploaded)
  * Upload two years of changes from CVS. Closes: #203031, #186504, #169589
    Closes: #226676, #179978
  * Fix a few typos.
  * Uses debhelper v3, so update build dependency version.

 -- Joey Hess <joeyh@debian.org>  Thu,  5 Aug 2004 14:24:20 -0400

debian-history (2.4) unstable; urgency=low

  * Build system cleanups by Josip Rodin.
  * Added new translations from CVS.

 -- Bdale Garbee <bdale@gag.com>  Tue, 15 Jul 2003 20:15:09 +0200

debian-history (2.3) unstable; urgency=low

  * quick update to reflect latest DPL election results

 -- Bdale Garbee <bdale@gag.com>  Sun, 21 Apr 2002 20:49:56 -0600

debian-history (2.2) unstable; urgency=low

  * fix typos pointed out by Oohara Yuuma <oohara@libra.interq.or.jp>

 -- Bdale Garbee <bdale@gag.com>  Sun, 17 Feb 2002 09:25:58 -0700

debian-history (2.1) unstable; urgency=low

  * since the package is so small, go back to delivering just one package but
    including all translations, instead of separate packages per language

 -- Bdale Garbee <bdale@gag.com>  Sat, 12 Jan 2002 21:29:38 -0700

debian-history (2.0) unstable; urgency=low

  * clean up lintian complaints
  * merge with project-history tree in CVS, deliver translations, etc.  roll
    version number far enough forward to eliminate confusion, closes: #74801

 -- Bdale Garbee <bdale@gag.com>  Fri,  4 Jan 2002 01:15:01 -0700

debian-history (0.1) unstable; urgency=low

  * Initial Release.
  * Many thanks to Will Lowe <lowe@debian.org> for his work on a project
    history, and for agreeing to let me take over the effort.  This version
    includes some minor additions and changes from Will's last version, and is
    far from what I would consider "complete".  I'm going to go ahead and
    upload it, both to ensure that the package is included in the 2.2 potato
    release, and to encourage others to provide inputs and feedback to help
    make this as accurate and complete as possible.  I intend to update the
    text substantially before potato finally releases.

 -- Bdale Garbee <bdale@gag.com>  Wed, 29 Dec 1999 01:37:16 -0700
