#######################################################################
# Makefile for the Debian History
# vim: set ts=8:
#######################################################################
### key adjustable parameters
#######################################################################
# base file name excluding file extension
MANUAL := project-history
# languages which has PO files
# debian/rules may reduce PO files used through this variable
LANGPO := $(patsubst po4a/po/%.po,%,$(wildcard po4a/po/??.po))
# languages to skip generation of PDF files (not used now)
NOPDF	:=
# all languages to build document
LANGALL	:= en $(LANGPO)
# All PO files
POFILES := $(patsubst %,po4a/po/%.po,$(LANGPO))
# All DocBook files from PO files
DBKFILES := $(patsubst %,$(MANUAL).%.dbk,$(LANGPO))

# Change $(DRAFTMODE) from "yes" to "maybe" when this document
# should go into production mode
#DRAFTMODE      := yes
DRAFTMODE       := maybe
export DRAFTMODE
#######################################################################
### basic constant parameters
#######################################################################
# Directories (no trailing slash)
DXSL	:=	xslt
DBIN	:=	bin
DPO	:=	po4a/po
DIMG	:=	/usr/share/xml/docbook/stylesheet/nwalsh/images

# Program name and option
XPINC	:=	xsltproc --novalid --nonet --xinclude
MSGATTR	:=	msgattrib
MSGCAT	:=	msgcat
DBLATEX	:=	dblatex

#######################################################################
# work around https://bugs.debian.org/725931
PERL_PERTURB_KEYS := 0
export PERL_PERTURB_KEYS
PERL_HASH_SEED := 0
export PERL_HASH_SEE
# BUILD_ROOT is where files are generated
BUILD_ROOT	:= $(CURDIR)/build_root
# Just in case
DESTDIR		?= $(CURDIR)/install_root
#######################################################################
# Used as $(call check-command, <command>, <package>)
define check-command
set -e; if ! which $(1) >/dev/null; then \
  echo "Missing command: $(1), install package: $(2)"; \
  false; \
fi
endef
#######################################################################
# This let you check source tree with "make"
.PHONY: all
all: css html
	$(DBIN)/fuzzypo $(DPO)/$*.po >>fuzzy.log
	cat fuzzy.log

.PHONY: build install
build: css html txt pdf epub

install: build
	mkdir -p $(DESTDIR)/usr/share/doc/debian-history/docs/
	cp -a $(BUILD_ROOT)/. $(DESTDIR)/usr/share/doc/debian-history/docs/

#######################################################################
# version of this Debian package (debian/changelog)
PUBVERSION ?= $(shell { git describe --tags 2>/dev/null |sed "s/^tags\//vcs-/" ; })
# short date of this Debian package (debian/changelog)
PUBDATE ?= $(shell { date +'%Y-%m-%d' ; })

version.ent:
	echo '<!ENTITY pubversion "$(PUBVERSION)">' > $@
	echo '<!ENTITY pubdate "$(PUBDATE)">' >> $@

#######################################################################
.PHONY: dbk
dbk: $(DBKFILES) $(MANUAL).en.dbk version.ent

.PHONY: po
po: $(DBKFILES) $(MANUAL).en.dbk

# Always generate  po4a/po/$(MANUAL).pot from the source
# VCS may or may not include po4a/po/$(MANUAL).pot this way

$(DBKFILES): $(MANUAL).en.dbk $(POFILES)
	touch po4a/po/$(MANUAL).pot
	po4a --force po4a/po4a.cfg

#######################################################################
.PHONY: css
css:
	-rm -rf $(BUILD_ROOT)/images
	mkdir -p $(BUILD_ROOT)/images
	cp -f $(DXSL)/$(MANUAL).css $(BUILD_ROOT)/$(MANUAL).css
	echo "AddCharset UTF-8 .txt" > $(BUILD_ROOT)/.htaccess
	cd $(DIMG) ; cp caution.png important.png note.png tip.png up.gif \
		warning.png $(BUILD_ROOT)/images
	cd png ; cp home.png next.png prev.png $(BUILD_ROOT)/images

#######################################################################
.PHONY: html
html:	$(foreach LX, $(LANGALL), $(BUILD_ROOT)/index.$(LX).html)

# Multi page HTML (target file name should be the full path from /)
$(BUILD_ROOT)/index.%.html: $(MANUAL).%.dbk version.ent
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_ROOT)
	$(XPINC)   --stringparam base.dir $(BUILD_ROOT)/ \
                --stringparam html.ext .$*.html \
                $(DXSL)/style-html.xsl $<

#######################################################################
.PHONY: txt
txt:	$(foreach LX, $(LANGALL), $(BUILD_ROOT)/$(MANUAL).$(LX).txt.gz)

# style-txt.xsl provides work around for hidden URL links by appending them
# explicitly.
$(BUILD_ROOT)/$(MANUAL).%.txt.gz: $(MANUAL).%.dbk version.ent
	@$(call check-command, w3m, w3m)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_ROOT)
	$(XPINC) $(DXSL)/style-txt.xsl $< | LC_ALL=en_US.UTF-8 \
	  w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html | \
	  gzip -n -9 - > $@

#######################################################################
.PHONY: pdf
pdf:	$(foreach LX, $(LANGALL), $(BUILD_ROOT)/$(MANUAL).$(LX).pdf)

nopdf: $(BUILD_ROOT)/nopdf.pdf

$(BUILD_ROOT)/nopdf.pdf: nopdf.tex
	-mkdir -p $(BUILD_ROOT)
	cd "$(CURDIR)/tmp/"; \
	xelatex ../nopdf.tex

$(foreach LX, $(NOPDF), $(BUILD_ROOT)/$(MANUAL).$(LX).pdf): $(BUILD_ROOT)/nopdf.pdf
	-mkdir -p $(BUILD_ROOT)
	cp $(BUILD_ROOT)/nopdf.pdf $@

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_ROOT)/$(MANUAL).%.pdf: $(MANUAL).%.dbk version.ent
	@$(call check-command, dblatex, dblatex)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(CURDIR)/tmp
	export TEXINPUTS=".:"; \
	export BUILD_ROOT="$(CURDIR)/tmp/"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
.PHONY: tex
tex:	$(foreach LX, $(LANGALL), $(BUILD_ROOT)/$(MANUAL).$(LX).tex)

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_ROOT)/$(MANUAL).%.tex: $(MANUAL).%.dbk version.ent
	-mkdir -p $(CURDIR)/tmp
	export TEXINPUTS=".:"; \
	export BUILD_ROOT="$(CURDIR)/tmp/"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--type=tex \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
.PHONY: epub
epub:	$(foreach LX, $(LANGALL), $(BUILD_ROOT)/$(MANUAL).$(LX).epub)

$(BUILD_ROOT)/$(MANUAL).%.epub: $(MANUAL).%.dbk version.ent
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_ROOT)/epub-tmp-$*/
	cd $(BUILD_ROOT)/epub-tmp-$*/ ; $(XPINC) $(CURDIR)/$(DXSL)/style-epub.xsl $(CURDIR)/$<
	cp -f $(DXSL)/mimetype $(BUILD_ROOT)/epub-tmp-$*/mimetype
	cp -f $(DXSL)/project-history.css $(BUILD_ROOT)/epub-tmp-$*/OEBPS/project-history.css
	cp -f $(DXSL)/debian-openlogo.png $(BUILD_ROOT)/epub-tmp-$*/OEBPS/debian-openlogo.png
	cd $(BUILD_ROOT)/epub-tmp-$*/ ; zip -r $@ ./
	rm -rf $(BUILD_ROOT)/epub-tmp-$*/

#######################################################################
.PHONY: clean
clean:
	-rm -f $(DBKFILES) version.ent
	-rm -rf $(BUILD_ROOT)
	-rm -f $(DPO)/*~ $(DPO)/*.mo
	-rm -f fuzzy.log
#######################################################################
.PHONY: distclean
distclean: clean

#######################################################################
### Utility targets
#######################################################################
.PHONY: wrap nowrap wip
wrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX ;\
	done
nowrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	done

wip:
	@$(call check-command, msgattrib, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) -o $$XX.fuzz --fuzzy        $$XX ;\
	$(MSGATTR) -o $$XX.untr --untranslated $$XX ;\
	done

#######################################################################
# $ make rsync
# export build result to http://people.debian.org/~osamu/debian-history/
#######################################################################
.PHONY: rsync
rsync: all
	rsync -avz $(BUILD_ROOT)/ osamu@people.debian.org:public_html/debian-history/

